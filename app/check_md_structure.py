#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import sys


def check_md_structure(file_path):
    with open(file_path, 'r') as file:

        file_contents = file.read()
        regex = ("title:\s*([^\s]+(?:\s+[^\s]+)*(?:\s+)?)(?=\ssubtitle:)",
                 "subtitle:\s*([^\s]+(?:\s+[^\s]+)*(?:\s+)?)(?=\sauthor:)",
                 "author:\s*([^\s]+(?:\s+[^\s]+)*(?:\s+)?)(?=\sauthor_image:)",
                 "date:\s*(\w+\s+\d{1,2},\s+\d{4})",
                 "image:\s*[^\s]+\.(?:png|jpe?g|gif)",
                 "permalink:\s*([^\s]+\S+)(?=\stags)",
                 "tags:\s*([^\s][\w\s-]+(?:,[\w\s-]+)*)(?=\sshortcontent)",
                 "shortcontent:\s*([^\s]+(?:\s+[^\s]+)*(?:\s+)?)(?=\s---)",
                 "---",
                 "(.*\s*)*"
                 )

        for rgx in regex:
            if re.search(rgx, file_contents, re.DOTALL):
                continue
            else:
                return False

        return True


if __name__ == '__main__':
    # Controlla se è stato passato il percorso del file come argomento da command line
    if len(sys.argv) > 1:
        file_path = sys.argv[1]
        match_found = check_md_structure(file_path)

        if match_found:
            print("Il file rispetta la struttura.")
        else:
            print("Il file non rispetta la struttura.")
            exit(1)
    else:
        print("Devi passare il percorso del file come argomento da command line.")
        exit(1)