#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
  echo "db in avvio"

  while ! nc -z db 5432; do
    sleep 0.1
  done

  python /app/manage.py create_db
  echo "PostgreSQL started"

fi

gunicorn --config /app/gunicorn_config.py wsgi:app